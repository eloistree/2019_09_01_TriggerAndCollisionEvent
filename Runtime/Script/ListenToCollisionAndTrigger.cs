﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ListenToCollisionAndTrigger : MonoBehaviour
{
    public enum TagLayerCondition { And, Or, All, JustTag, JustLayer }
    public enum CollisionType { RootofCollision, RealCollision }

    public bool m_listenToTrigger=true;
    public bool m_listenToCollision=true;
    public LayerMask m_masks=-1;
    public TagLayerCondition m_condition = TagLayerCondition.Or;
    public string[] m_tags;
//  public CollisionType m_collisionType = CollisionType.RealCollision;



    //public Dictionary<string, Collider> m_colliders = new Dictionary<string, Collider>();
    //public Dictionary<string, Collision> m_collisions = new Dictionary<string, Collision>();
    public Dictionary<string, GameObject> m_objectHit = new Dictionary<string, GameObject>();
    public GameObject[] m_collidingObject;
    public Rigidbody [] m_collidingRigidbody;


    public bool m_refreshListInUpdate =true;
    public bool m_refreshListWhenChanged;
    public void Update()
    {
        if(m_refreshListInUpdate)
        Refresh();
    }

    private void Refresh()
    {
        m_collidingObject = m_objectHit.Values.ToArray();
        m_collidingRigidbody = m_objectHit.Values.Select(k => k.GetComponent<Rigidbody>()).Where(k => k != null).ToArray();
    }

    public static bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));
    }

    public bool IsAllow(string[] tags, GameObject obj)
    {
        return tags.Contains(obj.tag);
    }
    public bool IsAllow(LayerMask layer, GameObject obj)
    {
        return IsInLayerMask(obj.layer, layer);

    }
    public bool IsAllowWithTags( GameObject obj)
    {
        return IsAllow( m_tags,  obj);
    }
    public bool IsAllowWithLayer( GameObject obj)
    {

        return IsAllow( m_masks, obj);
    }



    private void AddObject(string id ,GameObject obj)
    {
        if (!m_objectHit.ContainsKey(id)){
            m_objectHit.Add(id, obj);
        if (m_refreshListWhenChanged)
            Refresh();
        }
    }
    private void RemoveObject(string id)
    {
        if (m_objectHit.ContainsKey(id)){
            m_objectHit.Remove(id);
        if (m_refreshListWhenChanged)
            Refresh();
        }
    }

    private bool IsObjectListened(GameObject obj)
    {
        if (m_condition == TagLayerCondition.And)
            return IsAllowWithLayer(obj) && IsAllowWithTags(obj);
        if (m_condition == TagLayerCondition.Or)
            return IsAllowWithLayer(obj) || IsAllowWithTags(obj);
        if (m_condition == TagLayerCondition.JustLayer)
            return IsAllowWithLayer(obj);
        if (m_condition == TagLayerCondition.JustTag)
            return IsAllowWithTags(obj);

        return true;
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (!m_listenToTrigger)
            return;
        CheckIfAllowThenAdd(collision.gameObject);
    }

    private void OnTriggerExit(Collider collision)
    {
        
        RemoveObject("" + collision.gameObject.GetInstanceID());
    }

    private void OnTriggerStay(Collider collision)
    {
        
        if (!m_listenToTrigger)
            return;
        CheckIfAllowThenAdd(collision.gameObject);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (!m_listenToCollision)
            return;

        CheckIfAllowThenAdd(GetRealContact(collision));
    }

    private void OnCollisionExit(Collision collision)
    {
        
        RemoveObject("" + GetRealContact(collision).GetInstanceID());
    }

    private void OnCollisionStay(Collision collision)
    {
        if (!m_listenToCollision)
            return;
       
        CheckIfAllowThenAdd(GetRealContact(collision));
    }

    private GameObject GetRealContact(Collision collision)
    {
        if (collision == null)
            return null;

        //if (m_collisionType == CollisionType.RootofCollision)
            return collision.gameObject;
        //return collision.GetContact(0).thisCollider.gameObject;
    }
  
    private bool CheckIfAllowThenAdd(GameObject obj)
    {
        bool isAllow = IsObjectListened(obj);
        if (!isAllow)
            return false ;
        AddObject(""+obj.GetInstanceID(), obj);
        return true;
    }

    private bool CheckIfAllowThenRemove(GameObject obj)
    {
        bool isAllow = IsObjectListened(obj);
        if (!isAllow)
            return false;
        RemoveObject("" + obj.GetInstanceID());
        return true;
    }
   
}
